package overtime

import org.hibernate.validator.constraints.Email

class User {
    String login
    String firstName
    String lastName
    String password
    Email email

    static constraints = {
        login size: 2..15, blank: false, unique: true
        password size: 3..15, blank: false
        firstName size: 2..15, blank: false, unique: false
        lastName size: 2..15, blank: false, unique: false
        email email: true, blank: false
    }
}
